<?php

/**
 * @file
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * @file
 * Token.inc implementation hooks for Site Settings Advanced Tokens module.
 */

/**
 * Implements hook_token_info_alter().
 */
function site_settings_advanced_tokens_token_info_alter(&$data) {
  $site_settings_tokens = $data['tokens']['site_settings'];
  $storage = \Drupal::entityTypeManager()->getStorage('site_setting_entity');
  $type = \Drupal::entityTypeManager()->getStorage('site_setting_entity_type');
  $entity_types = $type->getQuery()->accessCheck()->execute();

  foreach ($entity_types as $entity_type) {
    $ids = $storage->getQuery()
      ->condition('type', $entity_type)
      ->accessCheck()
      ->execute();
    $entities = $storage->loadMultiple($ids);

    foreach (array_values($entities) as $key => $entity) {
      $fieldset_key = strtolower(str_replace(' ', '_', $entity->fieldset->getString()));
      $site_settings_tokens[$fieldset_key . ':' . $entity->bundle() . (count(array_values($entities)) > 1 ? ':' . $key : '')] = [
        'name' => $entity->label(),
        'description' => t('Field values from site settings %label', ['%label' => $entity->label()]),
        'type' => 'site_setting_entity',
      ];
    }

  }

  $data['tokens']['site_settings'] = $site_settings_tokens;

}

/**
 * Implements hook_tokens().
 */
function site_settings_advanced_tokens_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'site_settings') {
    $storage = \Drupal::entityTypeManager()->getStorage('site_setting_entity');
    foreach ($tokens as $name => $original) {
      $parts = explode(':', $name);

      $fieldset = array_shift($parts);
      $site_setting_name = array_shift($parts);
      if ($site_setting_name) {
        $index = is_numeric($parts[0]) ? $parts[0] : 0;

        $ids = $storage->getQuery()
          ->condition('type', $site_setting_name)
          ->sort('id')
          ->accessCheck()
          ->execute();

        $id = $ids ? array_keys($ids)[$index] : NULL;
        if ($id) {
          $site_setting_entity = $storage->load($id);
          unset($tokens[$name]);
          $tokens[implode(':', array_merge([$site_setting_name], $parts))] = $original;

          if ($entity_tokens = \Drupal::token()->findWithPrefix($tokens, $site_setting_name)) {
            $replacements += \Drupal::token()->generate('site_setting_entity', $entity_tokens, ['site_setting_entity' => $site_setting_entity], $options, $bubbleable_metadata);
          }
        }
      }

    }
  }
  return $replacements;
}
