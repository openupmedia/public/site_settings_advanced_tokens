# Site settings advanced tokens

This module extends the [Site settings and labels](https://www.drupal.org/project/site_settings) module 
with additional entity tokens.

## Requirements
This module requires the contrib [Site settings and labels](https://www.drupal.org/project/site_settings) module.

## Installation
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/extending-drupal Installing modules for 
further information.

```shell
composer require drupal/site_settings_advanced_tokens:dev-main
drush en site_settings_advanced_tokens
```


### Extra entity tokens
This module allows to get the site_setting_entity field with entity tokens.
When you have for example a site settings entity 'default_image' in the 'defaults' fieldset, which has the media entity reference field 'field_media' to add a default metatags image. You can get the img url with the following token: [site_settings:defaults:default_image:field_media:entity:field_media_image:entity:url]


